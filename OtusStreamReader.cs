﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.Collections;
using DZ1_Interfaces;
using System.IO;

namespace Homework1
{
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {

        ISerializer<T> Serializer;
        int position = -1;
        T[] arr ;

        //  IEnumerator<T> Enumerator;

        public OtusStreamReader (string DeserializeString, ISerializer<T> serializer)
        {
            Serializer = serializer;
            arr = Serializer.Deserialize(DeserializeString);
        }

        public T[] Deserialize(string  XmlString)
        {
            return Serializer.Deserialize(XmlString);
        }

        public void Dispose()
        {
            Serializer=null;
            arr = null;
        }

        public IEnumerator<T> GetEnumerator()
        {               
            for(int i = 0; i < arr.Length; i++)
            {                       
                yield return arr[i];
            }               
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
   
        public object Current
        {
            get
            {
                if(position == -1 || position >= arr.Length)
                    throw new IndexOutOfRangeException();
                return arr[position];
            }
        }

        public bool MoveNext()
        {
            if(position < arr.Length - 1)
            {
                position++;
                return true;
            }
            else
                return false;
        }

        public void Reset()
        {
            position = -1;
        }
    }
}
