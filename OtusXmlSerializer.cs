﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.IO;
using System.Xml;
using DZ1_Interfaces;

namespace Homework1
{
    public class OtusXmlSerializer<T> : ISerializer<T>  // Класс сериализатора
    {

        string ISerializer<T>.Serialize(T[] item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
             .UseAutoFormatting()
             .UseOptimizedNamespaces()
             .EnableImplicitTyping(typeof(T[]))
             .Create();


            return serializer.Serialize(item);
        }

        T[] ISerializer<T>.Deserialize(string str)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
            .UseAutoFormatting()
            .UseOptimizedNamespaces()
            .EnableImplicitTyping(typeof(T[]))
            .Create();

            return serializer.Deserialize<T[]>(str);



        }

    }

}
