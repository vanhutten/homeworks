﻿using System;
using System.Collections.Generic;
using System.Text;
using DZ1_Interfaces;
using Homework1.Interfaces;
using NUnit.Framework;

namespace Homework1
{
    public class AccountService : IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        private readonly RepositoryService _repositoryService;
        public AccountService( RepositoryService repositoryService)
        { 
            _repositoryService = repositoryService;
        }
        
        public bool AddAccount(Person account)
        {
            //Проверка на 18 лет и что имя не пустое
            if ((account.GetAge() > 18)&&(account.Name.Length>0))
            {
                //Добавляет аккаунт
                _repositoryService.Add(account);
                return true;
            }
            else
            {
                return false;
            }
        }
    }

}
