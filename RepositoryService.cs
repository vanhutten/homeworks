﻿using DZ1_Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Homework1.Interfaces
{
    public class RepositoryService : IRepository<Person>, ISerializer<Person>
    {
        // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
        readonly string _filename;
        private readonly ISerializer<Person> serializer;
        public Person[] persons;
        private readonly OtusStreamReader<Person> reader;
        public RepositoryService(string filename)
        {
            if (filename != null)
            {
                _filename = filename;
                var DeserializedString = File.ReadAllText(filename);
                serializer = new OtusXmlSerializer<Person>();
                IEnumerable<Person> reader = new OtusStreamReader<Person>(DeserializedString, serializer);
            }
            else { throw new NullReferenceException(); }
        }

        public void Add(Person item)
        {
            //Извлекаем массив из файла и добавляем новый элемент и пихаем в файл новый 
            persons = Deserialize(File.ReadAllText(_filename));
            Person[] NewPersons = new Person[persons.Length + 1];
            for (int i = 0; i<persons.Length; i++)
            {
                NewPersons[i] = persons[i].Clone();
            }
            // Добавляем новый элемент в конец массива
            NewPersons[^1] = item;
            // Сериализуем и записываем массив в файл
            File.WriteAllText(_filename, Serialize(NewPersons));
            persons = NewPersons;
        }

        public string Serialize(Person[] item) => serializer.Serialize(item);

        public Person[] Deserialize(string str) => serializer.Deserialize(str);


        public IEnumerable<Person> GetAll()
        {
            return (IEnumerable<Person>)reader.GetEnumerator();
        }

        public IEnumerator<Person> GetEnumerator()
        {
            return reader.GetEnumerator();
        }

        public Person GetOne(Func<Person, bool> predicate)
        {
            return this.Where(predicate).FirstOrDefault();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
