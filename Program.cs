﻿using DZ1_Interfaces;
using System;
using System.IO;
using System.Linq;
using Moq;


namespace Homework1
{

    //    Примерно такая архитектура/классы должны быть:
    //OtusXmlSerializer<T> : ISerializer<T>
    //OtusStreamReader<T> : IEnumerable<T>, IDisposable
    //PersonSorter : ISorter<T>
    //Person


    //Более подробно:
    //* ISerializer<T>
    //Имеет два метода:
    //string Serialize<T>(T item);
    //    T Deserialize<T>(Stream stream);

    //    если лень работать со стримами, можете Deserialize заменить на:
    //T Deserialize<T>(string data);
    //    Но тогда в OtusStreamReader тоже используйте строки.


    //   * OtusXmlSerializer<T> : ISerializer<T>
    //   для реализации методов интерфейса используем этот пакет https://www.nuget.org/packages/ExtendedXmlSerializer/
    //Примеры работы описаны здесь https://extendedxmlserializer.github.io/documentation/conceptual/documents/The-Basics.html
    //Также на проекте с урок в гитхабе есть пример https://github.com/Veikedo/Otus.Interfaces/blob/master/Otus.Interfaces/2.%20Strategy.cs#L25

    //* OtusStreamReader<T>
    //   В конструкторе получаем Stream и ISerializer<T>.В методе GetEnumerator() вызываем serializer.Deserialize<T[]>(Stream) и проходимся в цикле по массиву, возвращая элементы с помощью yield return. 
    //Да, можно было бы просто вернуть массив, но мы заодно научимся работать с yield.


    //   * ISorter<T> 
    //имеет один метод
    //   IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems);


    //* PersonSorter : ISorter<Person>
    //реализует метод Sort<Person>.Можно просто вызвать метод Linq


    class Program
    {
        static void Main(string[] args)
        {
            string filename = "persons.xml";
            string path = System.IO.Directory.GetCurrentDirectory() + "\\" + filename;
            Person[] people = { new Person() { Name = "Doc", ID = 3, BirthDay = new DateTime(1987,12,4)}
            , new Person() { ID = 1, Name = "Marty McFly", BirthDay = new DateTime(2005,2,11) }
            , new Person() { ID = 2, Name = "Jason Statham" , BirthDay = new DateTime(1993,8,26) } };
            Console.WriteLine("Исходный массив Person");
            Console.WriteLine(ArrayToString(people));
            ISerializer<Person> serializer = new OtusXmlSerializer<Person>();
            // Сериализация
            string xml = serializer.Serialize(people);
            OtusStreamReader<Person> reader = new OtusStreamReader<Person>(xml, serializer);
            ISorter<Person> NotSortedPersons = new PersonSorter();
            IAlgorithm<Person> NotSortedPersons2 = new PersonSorter();

            File.WriteAllText(path, xml);
            // Десериализация
            string buffer = File.ReadAllText(path);
            var NewPersons = serializer.Deserialize(buffer);
            //Сортировка
            var SortedPersons = NotSortedPersons.Sort(NewPersons).ToArray();
            var SortedPersons2 = NotSortedPersons2.Sort(NewPersons).ToArray();
            Console.WriteLine("Отсортированный методом ISorter.Sort()");
            Console.WriteLine(ArrayToString(SortedPersons));
            Console.WriteLine("Отсортированный методом IAlgorithm.Sort()");
            Console.WriteLine(ArrayToString(SortedPersons2));
            Console.ReadKey();

            //Задание 3

            AccountService accountService = new AccountService(new Interfaces.RepositoryService(filename));
            Console.Write("Введите Имя:");
            string NewName = Console.ReadLine();
            int NewID = ParseInt( "Введите новый ID:");

            int year = ParseInt("Введите год рождения:");
            int month = ParseInt("Введите месяц рождения:");
            int day = ParseInt("Введите день рождения:");
            accountService.AddAccount(new Person(NewName, NewID, new DateTime(year, month, day)));
            Console.WriteLine(ArrayToString(serializer.Deserialize(File.ReadAllText(filename))));
            //Mocking Не разобрался чего с ним делать 
            var mock = new Mock<IAccountService>();
            Person testPerson = new Person(NewName, 12, new DateTime(2014, 11, 2));
            mock.Setup(x => x.AddAccount(testPerson)).Returns(false);
            


        }

        public static string ArrayToString(Person[] people)
        {
            string str = "";
            foreach(Person person in people)
            {
                str += person.ToString() + "\n";
            }
            return str;
        }

        public static int ParseInt(string ConsoleString)
        {
            int digit;
            do
            {
                Console.Write(ConsoleString);
            } while(!int.TryParse(Console.ReadLine(), out digit));
            return digit;
        }

    }
}
