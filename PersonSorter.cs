﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DZ1_Interfaces;

namespace Homework1
{
    class PersonSorter : ISorter<Person> , IAlgorithm<Person>
    {
       

        IEnumerable<Person> ISorter<Person>.Sort(IEnumerable<Person> notSortedItems)
        {
            return notSortedItems.OrderBy(x => x.ID);          
        }

        // Еще один алгоритм сортировки
        IEnumerable<Person> IAlgorithm<Person>.Sort(IEnumerable<Person> notSortedItems)
        {
            Person temp;
            for(int i = 0; i < notSortedItems.Count(); i++)
            {             
                for (int j=i+1;j<notSortedItems.Count();j++)
                {
                    if(((Person[])notSortedItems)[i].ID > ((Person[])notSortedItems)[j].ID)
                    {
                        temp = ((Person[])notSortedItems)[i];
                        ((Person[])notSortedItems)[i] = ((Person[])notSortedItems)[j];
                        ((Person[])notSortedItems)[j] = temp;

                    }
                }
            }
            return notSortedItems;// Без сортировки
        }
    }
}
