﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework1
{
    public class Person 
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public DateTime BirthDay { get; set; }

        public Person(string name, int id, DateTime birthDate)
        {
            Name = name;
            ID = id;
            BirthDay = birthDate;
        }

        public Person()
        {
        }

        public override string ToString()
        {
            return "ID: " + this.ID.ToString() + " Name: " + this.Name + " Birthday: " + this.BirthDay.ToString();
        }

        public string ArrayToString(Person[] people)
        {
            string str="";
            foreach (Person person in people)
            {
                str += person.ToString() + "/n";
            }
            return str;
        }

        public Person Clone()
        {
            return new Person(this.Name, this.ID, this.BirthDay);
        }

        public int GetAge()
        {
            return DateTime.Now.Year - BirthDay.Year;
        }

    }
}
