﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DZ1_Interfaces
{
    public interface ISerializer<T>
    {
        public string Serialize (T[] item);
        public T[] Deserialize (string str);
    }
}
