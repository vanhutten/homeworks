﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ1_Interfaces
{
    public interface ISorter<T> 
    {
        public IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
    }
}
