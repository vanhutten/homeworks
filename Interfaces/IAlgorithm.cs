﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ1_Interfaces
{
    interface IAlgorithm<T> 
    {
        public IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
    }
}
