﻿using System;
using System.Collections.Generic;

namespace DZ1_Interfaces
{
    // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
    public interface IRepository<T> : IEnumerable<T>
    {
        // когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
        public IEnumerable<T> GetAll();
        public T GetOne(Func<T, bool> predicate);
        public void Add(T item);
    }
}
