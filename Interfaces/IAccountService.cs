﻿using System;
using System.Collections.Generic;
using System.Text;
using Homework1;

namespace DZ1_Interfaces
{
    interface IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
       public bool AddAccount(Person account);
    }
}
